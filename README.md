﻿# Weather
Тестовое задание.

- MVP
- Retrofit
- Локальное хранение прогноза на 5 дней в SQLite
- Работа оффлайн
- Поддержка планшетов
- Анимации
- Работа с RecyclerView - SwipeToDismiss, Drag&Drop

## Screenshots
![](screenshots/1.png)
![](screenshots/2.png)
![](screenshots/3.png)
![](screenshots/4.png)
![](screenshots/5.png)
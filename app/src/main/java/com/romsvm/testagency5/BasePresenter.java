package com.romsvm.testagency5;

import android.os.Bundle;

/**
 * Created by romsvm on 17.04.2017.
 */

public interface BasePresenter {
    void start();
}

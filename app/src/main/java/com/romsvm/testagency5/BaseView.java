package com.romsvm.testagency5;

/**
 * Created by romsvm on 17.04.2017.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
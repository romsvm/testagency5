package com.romsvm.testagency5;

import android.content.Context;
import android.support.annotation.NonNull;

import com.romsvm.testagency5.allcities.domain.usecase.CheckCity;
import com.romsvm.testagency5.allcities.domain.usecase.GetAllCities;
import com.romsvm.testagency5.allcities.domain.usecase.InitData;
import com.romsvm.testagency5.allcities.domain.usecase.ReorderCities;
import com.romsvm.testagency5.data.source.CitiesRepository;
import com.romsvm.testagency5.data.source.local.CitiesLocalDataSource;
import com.romsvm.testagency5.data.source.remote.RemoteData;
import com.romsvm.testagency5.details.domain.usecase.GetCity;
import com.romsvm.testagency5.details.domain.usecase.RemoveCity;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 17.04.2017.
 */

public class Injection {

    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }

    public static CitiesRepository provideCitiesRepository(@NonNull Context context) {
        checkNotNull(context);
        return CitiesRepository.getInstance(CitiesLocalDataSource.getInstance(context),
                RemoteData.getInstance());
    }

    public static InitData provideInitData(@NonNull Context context) {
        checkNotNull(context);
        return new InitData(provideCitiesRepository(context));
    }

    public static GetAllCities provideGetAllCities(@NonNull Context context) {
        checkNotNull(context);
        return new GetAllCities(provideCitiesRepository(context));
    }

    public static GetCity provideGetCity(@NonNull Context context) {
        checkNotNull(context);
        return new GetCity(provideCitiesRepository(context));
    }

    public static CheckCity provideCheckCity(@NonNull Context context) {
        checkNotNull(context);
        return new CheckCity(provideCitiesRepository(context));
    }

    public static RemoveCity provideRemoveCity(Context context) {
        checkNotNull(context);
        return new RemoveCity(provideCitiesRepository(context));
    }

    public static ReorderCities provideReorderCities(Context context) {
        checkNotNull(context);
        return new ReorderCities(provideCitiesRepository(context));
    }
}
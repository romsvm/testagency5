package com.romsvm.testagency5;

import android.os.Handler;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by romsvm on 17.04.2017.
 */

public class UseCaseThreadPoolScheduler implements UseCaseScheduler {

    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static final int QUEUE_SIZE = 10;
    private static final int TIMEOUT = 5000;

    //TODO RejectedExceptionHandler CustomBlockingQueue?
    public UseCaseThreadPoolScheduler() {

        ArrayBlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(QUEUE_SIZE);

        threadPoolExecutor = new ThreadPoolExecutor(NUMBER_OF_CORES, NUMBER_OF_CORES, TIMEOUT,
                TimeUnit.MILLISECONDS, blockingQueue,
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private ThreadPoolExecutor threadPoolExecutor;
    private final Handler handler = new Handler();

    @Override
    public void execute(Runnable runnable) {
        threadPoolExecutor.execute(runnable);
    }

    @Override
    public <V extends UseCase.ResponseValue> void notifyResponse(final V response,
                                                                 final UseCase.UseCaseCallback<V> useCaseCallback) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                useCaseCallback.onSuccess(response);
            }
        });
    }

    @Override
    public <V extends UseCase.ResponseValue> void onError(
            final UseCase.UseCaseCallback<V> useCaseCallback) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                useCaseCallback.onError();
            }
        });
    }
}



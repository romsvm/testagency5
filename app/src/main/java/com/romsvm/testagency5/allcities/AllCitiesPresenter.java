package com.romsvm.testagency5.allcities;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.UseCaseHandler;
import com.romsvm.testagency5.allcities.domain.usecase.CheckCity;
import com.romsvm.testagency5.allcities.domain.usecase.GetAllCities;
import com.romsvm.testagency5.allcities.domain.usecase.ReorderCities;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.details.domain.usecase.RemoveCity;

import java.util.HashMap;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 17.04.2017.
 */

public class AllCitiesPresenter implements AllCitiesContract.Presenter {

    private final UseCaseHandler useCaseHandler;
    private AllCitiesContract.View allCitiesView;
    private final GetAllCities ucGetAllCities;
    private final CheckCity ucCheckCity;
    private final RemoveCity ucRemoveCity;
    private final ReorderCities ucReorderCities;
    private boolean isGetAllCitiesInProgress = false;

    public AllCitiesPresenter(@NonNull UseCaseHandler useCaseHandler,
                              @NonNull AllCitiesContract.View allCitiesView,
                              @NonNull GetAllCities ucGetAllCities,
                              @NonNull CheckCity ucCheckCity,
                              @NonNull RemoveCity ucRemoveCity,
                              @NonNull ReorderCities ucReorderCities) {
        this.useCaseHandler = checkNotNull(useCaseHandler);
        this.allCitiesView = checkNotNull(allCitiesView);
        this.ucGetAllCities = checkNotNull(ucGetAllCities);
        this.ucCheckCity = checkNotNull(ucCheckCity);
        this.ucRemoveCity = checkNotNull(ucRemoveCity);
        this.ucReorderCities = checkNotNull(ucReorderCities);

        this.allCitiesView.setPresenter(this);
    }

    @Override
    public void start() {
        getAllCities();
    }

    @Override
    public void getAllCities() {
        if (!isGetAllCitiesInProgress) {
            isGetAllCitiesInProgress = true;
            useCaseHandler.execute(ucGetAllCities, new GetAllCities.RequestValues(),
                    new UseCase.UseCaseCallback<GetAllCities.ResponseValue>() {
                        @Override
                        public void onSuccess(GetAllCities.ResponseValue response) {
                            List<City> cities = response.getCities();
                            HashMap<Integer, List<Forecast>> forecasts = response.getForecasts();

                            HashMap<Integer, Forecast> dayForecast =
                                    new HashMap<Integer, Forecast>();

                            for (Integer cityId : forecasts.keySet()) {
                                dayForecast.put(cityId, forecasts.get(cityId).get(0));
                            }

                            if (allCitiesView.isActive()) {
                                allCitiesView.showAllCities(cities, dayForecast);
                            }

                            isGetAllCitiesInProgress = false;
                        }

                        @Override
                        public void onError() {
                            if (allCitiesView.isActive()) {
                                allCitiesView.showErrorAllCities();
                            }

                            isGetAllCitiesInProgress = false;
                        }
                    });
        } else {
            Log.d("MY", "getAllCities уже выполняется");
        }
    }

    @Override
    public void checkAndAddCity(final String cityName) {
        useCaseHandler.execute(ucCheckCity, new CheckCity.RequestValues(cityName),
                new UseCase.UseCaseCallback<CheckCity.ResponseValue>() {
                    @Override
                    public void onSuccess(CheckCity.ResponseValue response) {
                        getAllCities();

                        if (allCitiesView.isActive()) {
                            allCitiesView.showAddCityComplete();
                        }
                    }

                    /*Город с введенным именем отсутствует*/
                    @Override
                    public void onError() {
                        if (allCitiesView.isActive()) {
                            allCitiesView.showErrorCheckCity();
                        }
                    }
                });
    }

    @Override
    public void removeCity(int cityId) {
        useCaseHandler.execute(ucRemoveCity, new RemoveCity.RequestValues(cityId),
                new UseCase.UseCaseCallback<RemoveCity.ResponseValue>() {
                    @Override
                    public void onSuccess(RemoveCity.ResponseValue response) {
                        if (allCitiesView.isActive()) {
                            allCitiesView.showRemoveCityComplete();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void reorderCities(int fromPosition, int toPosition) {
        useCaseHandler.execute(ucReorderCities,
                new ReorderCities.RequestValues(fromPosition, toPosition),
                new UseCase.UseCaseCallback<ReorderCities.ResponseValue>() {
                    @Override
                    public void onSuccess(ReorderCities.ResponseValue response) {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }
}

package com.romsvm.testagency5.allcities;

import com.romsvm.testagency5.BasePresenter;
import com.romsvm.testagency5.BaseView;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;

import java.util.HashMap;
import java.util.List;

/**
 * Created by romsvm on 17.04.2017.
 */

public interface AllCitiesContract {
    interface View extends BaseView<Presenter> {
        /*Показываю данные для всего списка городов.*/
        void showAllCities(List<City> cities, HashMap<Integer, Forecast> forecasts);

        /*Показываю ошибку, если получить данные с сервера или из кэша не удалось.*/
        void showErrorAllCities();

        /*Показываю данные для выбранного города.*/
        void showSelectedCity(City city);

        /*Показываю диалог добавления города.*/
        void showAddCity();

        /*Показываю снэкбар при успешном добавлении города*/
        void showAddCityComplete();

        /*Показываю снэкбар при успешном удалении города*/
        void showRemoveCityComplete();

        /*Показываю сообщение об ошибке если не удалось записать в базу данных.*/
        void showErrorAddCity();

        /*Показываю сообщение об ошибке если при добавлении города если города с таким названием
        * нет на сервере*/
        void showErrorCheckCity();

        /*Показываю сообщение об ошибке если при добавлении города нет подключения к Интернет*/
        void showErrorInternet();

        /*true если фрагмент добавлен в активити*/
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        /*Получаю данные для всего списка городов.*/
        void getAllCities();

        /*Добавляю новый город, если такой есть на openweathermap.*/
        void checkAndAddCity(String cityName);

        void removeCity(int cityId);

        void reorderCities(int fromPosition, int toPosition);
    }
}
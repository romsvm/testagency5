package com.romsvm.testagency5.allcities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.romsvm.testagency5.R;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.details.DetailsActivity;
import com.romsvm.testagency5.details.DetailsFragment;
import com.romsvm.testagency5.details.ViewState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by romsvm on 17.04.2017.
 */

public class AllCitiesFragment extends Fragment implements AllCitiesContract.View,
                                                           AddCityDialogFragment.AddCityDialogListener {

    public static int REQUEST_CODE = 1;

    public static AllCitiesFragment newInstance() {
        return new AllCitiesFragment();
    }

    private AllCitiesContract.Presenter presenter;
    RecyclerView rvCities;
    CitiesListAdapter adapter;
    FloatingActionButton fab;

    public AllCitiesFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new CitiesListAdapter(new ArrayList<City>(0));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_all_cities, container, false);

        rvCities = (RecyclerView) root.findViewById(R.id.cities_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCities.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration
                .setDrawable(
                        getContext().getResources().getDrawable(R.drawable.cities_list_divider));
        rvCities.addItemDecoration(dividerItemDecoration);
        rvCities.setAdapter(adapter);

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab_add_city);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager =
                        (ConnectivityManager) getContext()
                                .getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

                if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
                    showAddCity();
                } else {
                    showErrorInternet();
                }
            }
        });

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.END) {

                    private Drawable background;
                    private Drawable icon;
                    boolean initiated;

                    private void init() {
                        background = new ColorDrawable(Color.RED);

                        Drawable raw =
                                ContextCompat.getDrawable(AllCitiesFragment.this.getActivity(),
                                        R.drawable.ic_delete);
                        icon = raw.mutate();
                        icon.setColorFilter(0xFFFFFFFF, PorterDuff.Mode.SRC_ATOP);
                        initiated = true;
                    }

                    @Override
                    public boolean isLongPressDragEnabled() {
                        return true;
                    }

                    @Override
                    public boolean isItemViewSwipeEnabled() {
                        return true;
                    }

                    @Override
                    public int getMovementFlags(RecyclerView recyclerView,
                                                RecyclerView.ViewHolder viewHolder) {
                        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                        int swipeFlags = ItemTouchHelper.END;
                        return makeMovementFlags(dragFlags, swipeFlags);
                    }

                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                                          RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        adapter.onItemMove(viewHolder.getAdapterPosition(),
                                target.getAdapterPosition());
                        return true;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        adapter.onItemDismiss(viewHolder.getAdapterPosition());
                    }

                    @Override
                    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                                            RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState,
                                            boolean isCurrentlyActive) {
                        View itemView = viewHolder.itemView;

                        if (viewHolder.getAdapterPosition() == -1) {
                            return;
                        }

                        if (!initiated) {
                            init();
                        }

                        // draw red background
                        background.setBounds(itemView.getLeft(), itemView.getTop(),
                                itemView.getLeft() + (int) dX, itemView.getBottom());
                        background.draw(c);

                        if (dX != 0) {
                            // draw x mark
                            int itemHeight = itemView.getBottom() - itemView.getTop();
                            int intrinsicWidth = icon.getIntrinsicWidth();
                            int intrinsicHeight = icon.getIntrinsicWidth();

                            int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                            int xMarkBottom = xMarkTop + intrinsicHeight;
                            int xMarkLeft = (itemHeight - intrinsicHeight) / 2 - intrinsicWidth / 4;
                            int xMarkRight = xMarkLeft + intrinsicWidth;

                            icon.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                            icon.draw(c);
                        }


                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState,
                                isCurrentlyActive);
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvCities);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.start();
    }

    @Override
    public void setPresenter(AllCitiesContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showAllCities(List<City> cities, HashMap<Integer, Forecast> forecasts) {
        adapter.replaceData(cities, forecasts);
    }

    @Override
    public void showErrorAllCities() {
        Toast toast = Toast.makeText(getContext(), R.string.error_all_cities, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showSelectedCity(City city) {
        ViewState.cityId = city.getId();
        if (getResources().getBoolean(R.bool.isTablet)) {
            DetailsFragment detailsFragment =
                    (DetailsFragment) getActivity().getSupportFragmentManager()
                                                   .findFragmentById(
                                                           R.id.contentFrameDetails);
            detailsFragment.getPresenter().getCity(ViewState.cityId);
        } else {
            Intent intent = new Intent(getContext(), DetailsActivity.class);
            startActivityForResult(intent, REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == DetailsFragment.RESPONSE_CODE) {
            Snackbar snackbar =
                    Snackbar.make(getView(), R.string.info_remove_city, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void showAddCity() {
        AddCityDialogFragment addCityDialogFragment = new AddCityDialogFragment();
        addCityDialogFragment.show(getChildFragmentManager(), "addCityDialogFragment");
    }

    @Override
    public void showAddCityComplete() {
        Snackbar snackbar = Snackbar.make(getView(), R.string.info_add_city, Snackbar.LENGTH_LONG);
        snackbar.show();

        rvCities.scrollToPosition(adapter.getItemCount());
    }

    @Override
    public void showRemoveCityComplete() {
        Snackbar snackbar =
                Snackbar.make(getView(), R.string.info_remove_city, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void showErrorAddCity() {
        Toast toast = Toast.makeText(getContext(), R.string.error_add_city, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showErrorCheckCity() {
        Toast toast = Toast.makeText(getContext(), R.string.error_check_city, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showErrorInternet() {
        Toast toast = Toast.makeText(getContext(), R.string.error_internet, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onAddCityDialogPositiveClick(String cityName) {
        Log.d("MY", cityName);
        presenter.checkAndAddCity(cityName);
    }

    @Override
    public void onAddCityDialogNegativeClick() {

    }

    public interface ItemTouchHelperAdapter {
        void onItemMove(int fromPosition, int toPosition);

        void onItemDismiss(int position);
    }

    private class CitiesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
            implements ItemTouchHelperAdapter {

        class CityViewHolder extends RecyclerView.ViewHolder {

            private TextView tvName;
            private TextView tvTemp;
            private TextView tvWeather;
            private ImageView imgWeather;

            CityViewHolder(View itemView) {
                super(itemView);

                this.tvName = (TextView) itemView.findViewById(R.id.title_city);
                this.tvTemp = (TextView) itemView.findViewById(R.id.tv_temp_item_list);
                this.tvWeather = (TextView) itemView.findViewById(R.id.tv_weather_item_list);
                this.imgWeather = (ImageView) itemView.findViewById(R.id.img_weather_item_list);
            }
        }

        private List<City> cities;
        private HashMap<Integer, Forecast> forecasts;

        CitiesListAdapter(List<City> cities) {
            this.cities = cities;
        }

        @Override
        public void onItemDismiss(int position) {
            City city = cities.get(position);
            cities.remove(city);
            forecasts.remove(city.getId());

            notifyItemRemoved(position);

            presenter.removeCity(city.getId());
        }

        @Override
        public void onItemMove(int fromPosition, int toPosition) {
            presenter.reorderCities(fromPosition, toPosition);
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(cities, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(cities, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
        }

        public void replaceData(List<City> cities, HashMap<Integer, Forecast> forecasts) {
            this.cities = cities;
            this.forecasts = forecasts;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_city, parent, false);
            final RecyclerView.ViewHolder viewHolder = new CityViewHolder(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int adapterPosition = viewHolder.getAdapterPosition();
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        City city = cities.get(adapterPosition);
                        showSelectedCity(city);
                    }
                }
            });

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            City city = cities.get(position);

            Forecast dayForecast = forecasts.get(city.getId());

            ((CityViewHolder) holder).tvName.setText(city.getName());
            ((CityViewHolder) holder).tvTemp.setText(
                    ((int) dayForecast.getTemp().getMin()) + "\u00b0 / " +
                    ((int) dayForecast.getTemp().getMax()) + "\u00b0");

            int res = getResources().getIdentifier("com.romsvm.testagency5:drawable/ic_" +
                                                   dayForecast.getWeather().get(0).getIcon()
                                                              .substring(0, 2), null, null);
            ((CityViewHolder) holder).imgWeather.setImageResource(res);
            ((CityViewHolder) holder).imgWeather.setImageAlpha(127);

            ((CityViewHolder) holder).tvWeather
                    .setText(dayForecast.getWeather().get(0).getDescription());
        }

        @Override
        public int getItemCount() {
            return cities.size();
        }
    }
}

package com.romsvm.testagency5.allcities.domain.usecase;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.source.CitiesDataSource;
import com.romsvm.testagency5.data.source.CitiesRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 20.04.2017.
 */

public class CheckCity extends UseCase<CheckCity.RequestValues, CheckCity.ResponseValue> {
    private final CitiesRepository citiesRepository;

    public CheckCity(@NonNull CitiesRepository citiesRepository) {
        this.citiesRepository = checkNotNull(citiesRepository);
    }

    @Override
    protected void executeUseCase(final CheckCity.RequestValues requestValues) {
        String cityName = requestValues.getCityName();
        citiesRepository.checkCity(cityName, new CitiesDataSource.CheckCityCallback() {
            @Override
            public void onCityLoaded(City city) {
                getUseCaseCallback().onSuccess(new CheckCity.ResponseValue(city));
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final String cityName;

        public RequestValues(@NonNull String cityName) {
            this.cityName = checkNotNull(cityName);
        }

        public String getCityName() {
            return cityName;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final City city;

        public ResponseValue(@NonNull City city) {
            this.city = checkNotNull(city);
        }

        public City getCity() {
            return city;
        }
    }
}

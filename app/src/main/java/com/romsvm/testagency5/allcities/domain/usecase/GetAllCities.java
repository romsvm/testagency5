package com.romsvm.testagency5.allcities.domain.usecase;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.data.source.CitiesDataSource;
import com.romsvm.testagency5.data.source.CitiesRepository;

import java.util.HashMap;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 18.04.2017.
 */

public class GetAllCities extends UseCase<GetAllCities.RequestValues, GetAllCities.ResponseValue> {

    private final CitiesRepository citiesRepository;

    public GetAllCities(@NonNull CitiesRepository citiesRepository) {
        this.citiesRepository = checkNotNull(citiesRepository);
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        citiesRepository.getAllCities(new CitiesDataSource.GetAllCitiesCallback() {
            @Override
            public void onCitiesLoaded(List<City> cities,
                                       HashMap<Integer, List<Forecast>> forecasts) {
                ResponseValue responseValue = new ResponseValue(cities, forecasts);
                getUseCaseCallback().onSuccess(responseValue);
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {

    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final List<City> cities;
        private final HashMap<Integer, List<Forecast>> forecasts;

        public ResponseValue(@NonNull List<City> cities,
                             @NonNull HashMap<Integer, List<Forecast>> forecasts) {

            this.cities = checkNotNull(cities);
            this.forecasts = checkNotNull(forecasts);
        }

        public List<City> getCities() {
            return cities;
        }

        public HashMap<Integer, List<Forecast>> getForecasts() {
            return forecasts;
        }
    }
}

package com.romsvm.testagency5.allcities;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;

import com.romsvm.testagency5.Injection;
import com.romsvm.testagency5.R;
import com.romsvm.testagency5.details.DetailsFragment;
import com.romsvm.testagency5.details.DetailsPresenter;
import com.romsvm.testagency5.util.ActivityUtils;
import com.romsvm.testagency5.util.EspressoIdlingResource;

/**
 * Created by romsvm on 17.04.2017.
 */

public class AllCitiesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_cities);

        AllCitiesFragment allCitiesFragment =
                (AllCitiesFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.contentFrame);
        if (allCitiesFragment == null) {
            // Create the fragment
            allCitiesFragment = AllCitiesFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), allCitiesFragment, R.id.contentFrame);
        }

        new AllCitiesPresenter(Injection.provideUseCaseHandler(), allCitiesFragment,
                Injection.provideGetAllCities(getApplicationContext()),
                Injection.provideCheckCity(getApplicationContext()),
                Injection.provideRemoveCity(getApplicationContext()),
                Injection.provideReorderCities(getApplicationContext()));

        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        if (isTablet) {
            DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.contentFrameDetails);

            if (detailsFragment == null) {
                detailsFragment = DetailsFragment.newInstance();
                ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), detailsFragment,
                        R.id.contentFrameDetails);
            }

            new DetailsPresenter(Injection.provideUseCaseHandler(), detailsFragment,
                    Injection.provideGetCity(getApplicationContext()),
                    Injection.provideRemoveCity(getApplicationContext()));
        }
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
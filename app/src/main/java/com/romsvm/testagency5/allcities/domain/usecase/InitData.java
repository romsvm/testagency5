package com.romsvm.testagency5.allcities.domain.usecase;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.data.source.CitiesDataSource;
import com.romsvm.testagency5.data.source.CitiesRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 18.04.2017.
 */

public class InitData extends UseCase<InitData.RequestValues, InitData.ResponseValue> {

    private final CitiesRepository citiesRepository;

    public InitData(@NonNull CitiesRepository citiesRepository) {
        this.citiesRepository = checkNotNull(citiesRepository);
    }

    @Override
    protected void executeUseCase(final RequestValues requestValues) {
        int[] cities = requestValues.getCities();
        citiesRepository.initData(cities, new CitiesDataSource.InitDataCallback() {
            @Override
            public void onInitiated() {
                getUseCaseCallback().onSuccess(new ResponseValue());
            }

            @Override
            public void onError() {
                getUseCaseCallback().onError();
            }
        });

    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final int[] cities;

        public RequestValues(@NonNull int[] cities) {
            this.cities = checkNotNull(cities);
        }

        public int[] getCities() {
            return cities;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {
        public ResponseValue() {
        }
    }
}

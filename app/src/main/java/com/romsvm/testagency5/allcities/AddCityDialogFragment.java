package com.romsvm.testagency5.allcities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.romsvm.testagency5.R;

/**
 * Created by romsvm on 22.04.2017.
 */

public class AddCityDialogFragment extends DialogFragment {
    public interface AddCityDialogListener {
        void onAddCityDialogPositiveClick(String cityName);

        void onAddCityDialogNegativeClick();
    }

    private AddCityDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            FragmentManager supportFragmentManager =
                    ((AllCitiesActivity) context).getSupportFragmentManager();
            Fragment fragment = supportFragmentManager.findFragmentById(R.id.contentFrame);

            listener = (AddCityDialogListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() + "must implement AddCityDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final View view =
                getActivity().getLayoutInflater().inflate(R.layout.dialog_add_new_city, null);
        final EditText editText = (EditText) view.findViewById(R.id.tv_name_new_city);

        final EditText tvCityName = (EditText) view.findViewById(R.id.tv_name_new_city);
        tvCityName.requestFocus();

        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
                .getSystemService(getContext().INPUT_METHOD_SERVICE);

        builder.setMessage(R.string.dialog_add_city_message);
        builder.setView(view);
        builder.setPositiveButton(R.string.dialog_add_city_message_positive,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onAddCityDialogPositiveClick(editText.getText().toString());
                        inputMethodManager.hideSoftInputFromWindow(tvCityName.getWindowToken(), 0);
                    }
                });
        builder.setNegativeButton(R.string.dialog_delete_city_message_negative,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onAddCityDialogNegativeClick();
                        inputMethodManager.hideSoftInputFromWindow(tvCityName.getWindowToken(), 0);
                    }
                });


        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        return builder.create();
    }
}

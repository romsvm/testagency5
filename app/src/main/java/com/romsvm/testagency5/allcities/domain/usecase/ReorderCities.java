package com.romsvm.testagency5.allcities.domain.usecase;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.data.source.CitiesRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 01.05.2017.
 */

public class ReorderCities
        extends UseCase<ReorderCities.RequestValues, ReorderCities.ResponseValue> {
    private final CitiesRepository citiesRepository;

    public ReorderCities(@NonNull CitiesRepository citiesRepository) {
        this.citiesRepository = checkNotNull(citiesRepository);
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        citiesRepository.reorderCities(requestValues.fromPosition, requestValues.toPosition);
    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final int fromPosition;
        private final int toPosition;

        public RequestValues(int fromPosition, int toPosition) {
            this.fromPosition = fromPosition;
            this.toPosition = toPosition;
        }

        public int getFromPosition() {
            return fromPosition;
        }

        public int getToPosition() {
            return toPosition;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        public ResponseValue() {
        }
    }
}

package com.romsvm.testagency5.details;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;

import com.romsvm.testagency5.Injection;
import com.romsvm.testagency5.R;
import com.romsvm.testagency5.util.ActivityUtils;
import com.romsvm.testagency5.util.EspressoIdlingResource;

/**
 * Created by romsvm on 17.04.2017.
 */
public class DetailsActivity extends AppCompatActivity {

    DetailsFragment detailsFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

         detailsFragment = (DetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrameDetails);


        if (detailsFragment == null) {
            detailsFragment = DetailsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), detailsFragment,
                    R.id.contentFrameDetails);
        }

        new DetailsPresenter(Injection.provideUseCaseHandler(), detailsFragment,
                        Injection.provideGetCity(getApplicationContext()),
                        Injection.provideRemoveCity(getApplicationContext()));
    }

    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        detailsFragment.animate();
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
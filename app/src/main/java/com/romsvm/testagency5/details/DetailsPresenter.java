package com.romsvm.testagency5.details;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.UseCaseHandler;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.details.domain.usecase.GetCity;
import com.romsvm.testagency5.details.domain.usecase.RemoveCity;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 17.04.2017.
 */

public class DetailsPresenter implements DetailsContract.Presenter {

    private final UseCaseHandler useCaseHandler;
    private DetailsContract.View detailsView;
    private final GetCity ucGetCity;
    private final RemoveCity ucRemoveCity;

    public DetailsPresenter(@NonNull UseCaseHandler useCaseHandler,
                             @NonNull DetailsContract.View detailsView,
                             @NonNull GetCity ucGetCity,
                             @NonNull RemoveCity ucRemoveCity) {
        this.useCaseHandler = checkNotNull(useCaseHandler);
        this.detailsView = checkNotNull(detailsView);
        this.ucGetCity = checkNotNull(ucGetCity);
        this.ucRemoveCity = checkNotNull(ucRemoveCity);

        this.detailsView.setPresenter(this);
    }

    @Override
    public void start() {
        getCity(ViewState.cityId);
    }

    @Override
    public void getCity(int cityId) {
        if (cityId == 0) {
            if (detailsView.isActive()) {
                detailsView.showSelectCityMessage();
            }
        } else {
            useCaseHandler.execute(ucGetCity, new GetCity.RequestValues(cityId),
                    new UseCase.UseCaseCallback<GetCity.ResponseValue>() {
                        @Override
                        public void onSuccess(GetCity.ResponseValue response) {
                            City city = response.getCity();
                            List<Forecast> listForecasts = response.getListForecast();

                            if (detailsView.isActive()) {
                                detailsView.showCity(city, listForecasts);
                            }
                        }

                        @Override
                        public void onError() {
                            if (detailsView.isActive()) {
                                detailsView.showErrorGetCity();
                            }
                        }
                    });
        }
    }

    @Override
    public void removeCity() {
        useCaseHandler.execute(ucRemoveCity, new RemoveCity.RequestValues(ViewState.cityId),
                new UseCase.UseCaseCallback<RemoveCity.ResponseValue>() {
                    @Override
                    public void onSuccess(RemoveCity.ResponseValue response) {
                        ViewState.cityId = 0;
                        if (detailsView.isActive()) {
                            detailsView.showAllCities();
                        }
                    }

                    @Override
                    public void onError() {
                        if (detailsView.isActive()) {
                            detailsView.showErrorRemoveCity();
                        }
                    }
                });
    }
}
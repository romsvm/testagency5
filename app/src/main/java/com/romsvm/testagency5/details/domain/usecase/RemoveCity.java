package com.romsvm.testagency5.details.domain.usecase;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.data.source.CitiesRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 19.04.2017.
 */

public class RemoveCity extends UseCase<RemoveCity.RequestValues, RemoveCity.ResponseValue> {

    private final CitiesRepository citiesRepository;

    public RemoveCity(@NonNull CitiesRepository citiesRepository) {
        this.citiesRepository = checkNotNull(citiesRepository);
    }

    @Override
    protected void executeUseCase(RemoveCity.RequestValues requestValues) {
        int cityId = requestValues.getCityId();
        citiesRepository.removeCity(cityId);
        getUseCaseCallback().onSuccess(new ResponseValue());
    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final int cityId;

        public RequestValues(@NonNull int cityId) {
            this.cityId = checkNotNull(cityId);
        }

        public int getCityId() {
            return cityId;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        public ResponseValue() {

        }
    }
}

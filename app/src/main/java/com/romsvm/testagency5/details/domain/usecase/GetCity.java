package com.romsvm.testagency5.details.domain.usecase;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.data.source.CitiesDataSource;
import com.romsvm.testagency5.data.source.CitiesRepository;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 18.04.2017.
 */

public class GetCity extends UseCase<GetCity.RequestValues, GetCity.ResponseValue> {
    private final CitiesRepository citiesRepository;

    public GetCity(@NonNull CitiesRepository citiesRepository) {
        this.citiesRepository = checkNotNull(citiesRepository);
    }

    @Override
    protected void executeUseCase(GetCity.RequestValues requestValues) {
        int cityId = requestValues.getCityId();
        citiesRepository.getCity(cityId, new CitiesDataSource.GetCityCallback() {
            @Override
            public void onCityLoaded(City city, List<Forecast> listForecasts) {
                getUseCaseCallback().onSuccess(new ResponseValue(city, listForecasts));
            }

            @Override
            public void onDataNotAvailable() {
                getUseCaseCallback().onError();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {
        private final int cityId;

        public RequestValues(@NonNull int cityId) {
            this.cityId = checkNotNull(cityId);
        }

        public int getCityId() {
            return cityId;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final City city;
        private final List<Forecast> listForecast;

        public ResponseValue(@NonNull City city, @NonNull List<Forecast> listForecast) {
            this.city = checkNotNull(city);
            this.listForecast = checkNotNull(listForecast);
        }

        public City getCity() {
            return city;
        }

        public List<Forecast> getListForecast() {
            return listForecast;
        }
    }
}

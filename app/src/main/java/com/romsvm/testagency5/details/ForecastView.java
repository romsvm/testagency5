package com.romsvm.testagency5.details;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.romsvm.testagency5.R;
import com.romsvm.testagency5.data.model.Forecast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by romsvm on 22.04.2017.
 */

public class ForecastView extends LinearLayout {

    TextView tvDayOfWeek;
    TextView tvDate;
    ImageView imgWeather;
    TextView tvWeather;
    TextView tvTemps;
    TextView tvWindDeg;
    TextView tvWindSpeed;
    Paint paint;

    String[] arr =
            {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"};


    public ForecastView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        inflate(getContext(), R.layout.forecast_view, this);

        tvDayOfWeek = (TextView) findViewById(R.id.tv_day_of_week);
        tvDate = (TextView) findViewById(R.id.tv_date);
        imgWeather = (ImageView) findViewById(R.id.img_weather);
        tvWeather = (TextView) findViewById(R.id.tv_weather);
        tvTemps = (TextView) findViewById(R.id.tv_temps);
        tvWindDeg = (TextView) findViewById(R.id.tv_wind_deg);
        tvWindSpeed = (TextView) findViewById(R.id.tv_wind_speed);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(0x05000000);

        setWillNotDraw(false);
    }

    public void setData(Forecast forecast) {

        /*Время с сервера приходит в секундах и хранится в int*/
        long dt = forecast.getDt() * 1000L;

        Calendar cForecast = Calendar.getInstance();
        cForecast.setTimeInMillis(dt);

        Calendar cToday = Calendar.getInstance();
        Calendar cTomorrow = Calendar.getInstance();
        cTomorrow.set(Calendar.DAY_OF_MONTH, cTomorrow.get(Calendar.DAY_OF_MONTH) + 1);

        if (new SimpleDateFormat("dd.MM.yyyy")
                .format(cForecast.getTimeInMillis())
                .equals(new SimpleDateFormat("dd.MM.yyyy")
                        .format(cToday.getTimeInMillis()))) {
            tvDayOfWeek.setText("Today");
        } else if (new SimpleDateFormat("dd.MM.yyyy")
                .format(cForecast.getTimeInMillis())
                .equals(new SimpleDateFormat("dd.MM.yyyy")
                        .format(cTomorrow.getTimeInMillis()))) {
            tvDayOfWeek.setText("Tomorrow");
        } else {
            tvDayOfWeek.setText(new SimpleDateFormat("EE", new Locale("EN")).format(dt));
        }

        tvDate.setText(new SimpleDateFormat("dd.MM").format(dt));
        tvWeather.setText(forecast.getWeather().get(0).getDescription());
        tvTemps.setText(((int) forecast.getTemp().getMin()) + "\u00b0 / " +
                        ((int) forecast.getTemp().getMax()) + "\u00b0");

        tvWindDeg.setText(degToCompass(forecast.getDeg()));
        tvWindSpeed.setText((int) forecast.getSpeed() + "m/s");

        int res = getResources().getIdentifier("com.romsvm.testagency5:drawable/ic_" +
                                               forecast.getWeather().get(0).getIcon()
                                                       .substring(0, 2), null, null);
        imgWeather.setImageResource(res);
        imgWeather.setImageAlpha(127);

        setWillNotDraw(!(new SimpleDateFormat("dd.MM.yyyy")
                .format(cForecast.getTimeInMillis())
                .equals(new SimpleDateFormat("dd.MM.yyyy")
                        .format(cToday.getTimeInMillis()))));
    }

    /*
    1. Divide the angle by 22.5 because 360deg/16 directions = 22.5deg/direction change.
    2. Add .5 so that when you truncate the value you can break the 'tie' between the change threshold.
    3. Truncate the value using integer division (so there is no rounding).
    4. Directly index into the array and print the value (mod 16).*/
    private String degToCompass(double value) {
        int val = (int) ((value / 22.5) + .5);
        return arr[(val % 16)];
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
    }
}

package com.romsvm.testagency5.details;

import com.romsvm.testagency5.BasePresenter;
import com.romsvm.testagency5.BaseView;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;

import java.util.List;

/**
 * Created by romsvm on 17.04.2017.
 */

public interface DetailsContract {
    interface View extends BaseView<Presenter> {
        /*Показываю информацию по выбранному городу*/
        void showCity(City city, List<Forecast> forecasts);

        /*Показываю ошибку чтения базы данных*/
        void showErrorGetCity();

        /*Показываю ошибку чтения-записи базы данных*/
        void showErrorRemoveCity();

        /*Показываю весь список городов - на телефоне возвращаюсь в главную активити, на планшете
        * обновляю текущую активити*/
        void showAllCities();

        /*На планшетах показываю сообщение "Выберите город" при старте приложения*/
        void showSelectCityMessage();

        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void getCity(int cityId);

        void removeCity();
    }
}
package com.romsvm.testagency5.details;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.romsvm.testagency5.R;

/**
 * Created by romsvm on 22.04.2017.
 */

public class DeleteCityDialogFragment extends DialogFragment {
    public interface DeleteCityDialogListener {
        void onDeleteCityDialogPositiveClick();

        void onDeleteCityDialogNegativeClick();
    }

    private DeleteCityDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            FragmentManager supportFragmentManager =
                    ((AppCompatActivity) context).getSupportFragmentManager();
            Fragment fragment = supportFragmentManager.findFragmentById(R.id.contentFrameDetails);

            listener = (DeleteCityDialogListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() + "must implement DeleteCityDialogFragment");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_delete_city_message);
        builder.setPositiveButton(R.string.dialog_delete_city_message_positive,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onDeleteCityDialogPositiveClick();
                    }
                });
        builder.setNegativeButton(R.string.dialog_delete_city_message_negative,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onDeleteCityDialogNegativeClick();
                    }
                });

        return builder.create();
    }
}

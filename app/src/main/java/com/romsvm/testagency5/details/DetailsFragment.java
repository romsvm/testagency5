package com.romsvm.testagency5.details;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.romsvm.testagency5.R;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by romsvm on 17.04.2017.
 */

public class DetailsFragment extends Fragment implements DetailsContract.View,
                                                         DeleteCityDialogFragment.DeleteCityDialogListener {

    public static final int RESPONSE_CODE = 1;

    public static DetailsFragment newInstance() {
        return new DetailsFragment();
    }

    private DetailsContract.Presenter presenter;
    private TextView tvCityName;
    private TextView tvAmountForecasts;
    private TextView tvTimePeriod;
    private LinearLayout llContent;
    private ImageButton button;

    private LinearLayout content;
    private TextView tvSelectCity;

    public DetailsFragment() {

    }

    public void animate() {
        llContent.removeAllViews();

        for (int i = 0; i < forecastViewList.size(); i++) {
             ForecastView forecastView = forecastViewList.get(i);
            llContent.addView(forecastView);

            forecastView.setAlpha(0);

            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(forecastView, "alpha", 1f);
            objectAnimator.setDuration(300);
            objectAnimator.setStartDelay(i * 100);
            objectAnimator.start();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_details, container, false);

        tvCityName = (TextView) root.findViewById(R.id.tv_city_name);
        tvAmountForecasts = (TextView) root.findViewById(R.id.tv_amount_forecasts);
        tvTimePeriod = (TextView) root.findViewById(R.id.tv_time_period);
        llContent = (LinearLayout) root.findViewById(R.id.contentLayout);
        button = (ImageButton) root.findViewById(R.id.button);

        content = (LinearLayout) root.findViewById(R.id.content);
        tvSelectCity = (TextView) root.findViewById(R.id.tv_select_city);

        if (getResources().getBoolean(R.bool.isTablet)) {
            tvSelectCity.setVisibility(View.INVISIBLE);
            content.setVisibility(View.VISIBLE);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteCityDialogFragment dialogFragment = new DeleteCityDialogFragment();
                dialogFragment.show(getChildFragmentManager(), "dialogDeleteCity");
            }
        });

        presenter.start();

        needExplicitAnimation = savedInstanceState != null;

        return root;
    }

    private boolean needExplicitAnimation = false;

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void setPresenter(DetailsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public DetailsContract.Presenter getPresenter() {
        return presenter;
    }

    private List<ForecastView> forecastViewList = new ArrayList<>();

    @Override
    public void showCity(City city, List<Forecast> forecasts) {

        tvCityName.setText(city.getName());
        tvAmountForecasts.setText(forecasts.size() + " day forecast");
        tvTimePeriod.setText(
                new SimpleDateFormat("dd.MM").format(forecasts.get(0).getDt() * 1000L) + " - " +
                new SimpleDateFormat("dd.MM")
                        .format(forecasts.get(forecasts.size() - 1).getDt() * 1000L));


        forecastViewList.clear();

        for (int i = 0; i < forecasts.size(); i++) {
            Forecast forecast = forecasts.get(i);
            ForecastView forecastView = new ForecastView(getActivity(), null);
            forecastView.setData(forecast);
            forecastViewList.add(forecastView);
        }

        if (needExplicitAnimation || getResources().getBoolean(R.bool.isTablet)) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    animate();
                }
            }, 500L);
        }

        if (getResources().getBoolean(R.bool.isTablet)) {
            tvSelectCity.setVisibility(View.INVISIBLE);
            content.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorGetCity() {
        Toast toast = Toast.makeText(getContext(), R.string.error_get_city, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void showAllCities() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            getActivity().recreate();
        } else {
            getActivity().setResult(DetailsFragment.RESPONSE_CODE);
            getActivity().finish();
        }
    }

    @Override
    public void showSelectCityMessage() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            tvSelectCity.setVisibility(View.VISIBLE);
            content.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showErrorRemoveCity() {
        Toast toast = Toast.makeText(getContext(), R.string.error_remove_city, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onDeleteCityDialogPositiveClick() {
        presenter.removeCity();
    }

    @Override
    public void onDeleteCityDialogNegativeClick() {

    }
}
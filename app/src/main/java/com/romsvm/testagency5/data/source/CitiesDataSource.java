package com.romsvm.testagency5.data.source;

import android.support.annotation.NonNull;

import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;

import java.util.HashMap;
import java.util.List;

/**
 * Created by romsvm on 17.04.2017.
 */

public interface CitiesDataSource {

    interface InitDataCallback {
        void onInitiated();

        void onError();
    }

    interface GetAllCitiesCallback {
        void onCitiesLoaded(List<City> cities, HashMap<Integer, List<Forecast>> forecasts);

        void onDataNotAvailable();
    }

    interface GetCityCallback {
        void onCityLoaded(City city, List<Forecast> listForecasts);

        void onDataNotAvailable();
    }

    interface CheckCityCallback {
        void onCityLoaded(City city);

        void onDataNotAvailable();
    }

    void getAllCities(@NonNull GetAllCitiesCallback callback);

    void getCity(@NonNull int cityId, @NonNull GetCityCallback callback);

    void addCity(City city, List<Forecast> listForecast);

    void checkCity(String cityName, @NonNull CheckCityCallback callback);

    void reorderCities(int fromPosition, int toPosition);

    void removeCity(int cityId);

    void removeAllCities();


}

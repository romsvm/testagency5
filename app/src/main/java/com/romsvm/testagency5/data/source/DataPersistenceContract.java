package com.romsvm.testagency5.data.source;

import android.provider.BaseColumns;

/**
 * Created by romsvm on 17.04.2017.
 */

public class DataPersistenceContract {

    private DataPersistenceContract() {

    }

    public static abstract class CityEntry implements BaseColumns {
        public static final String TABLE_NAME = "cities";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ORDER = "order_city";
        public static final String COLUMN_NAME_COORD_LON = "coord_lon";
        public static final String COLUMN_NAME_COORD_LAT = "coord_lat";
        public static final String COLUMN_NAME_COUNTRY = "country";
    }

    public static abstract class ForecastEntry implements BaseColumns {
        public static final String TABLE_NAME = "forecasts";

        public static final String COLUMN_NAME_CITY_ID = "cityId";
        public static final String COLUMN_NAME_DT = "dt";
        public static final String COLUMN_NAME_HUMIDITY = "humidity";
        public static final String COLUMN_NAME_PRESSURE = "pressure";
        public static final String COLUMN_NAME_CLOUDS = "clouds";
        public static final String COLUMN_NAME_SPEED = "wind_speed";
        public static final String COLUMN_NAME_DEG = "wind_deg";

        public static final String COLUMN_NAME_WEATHER_ID = "weather_id";
        public static final String COLUMN_NAME_WEATHER_MAIN = "weather_main";
        public static final String COLUMN_NAME_WEATHER_DESCRIPTION = "weather_description";
        public static final String COLUMN_NAME_WEATHER_ICON = "weather_icon";

        public static final String COLUMN_NAME_TEMP_DAY = "temp_day";
        public static final String COLUMN_NAME_TEMP_MAX = "temp_max";
        public static final String COLUMN_NAME_TEMP_MIN = "temp_min";
        public static final String COLUMN_NAME_TEMP_NIGHT = "temp_night";
        public static final String COLUMN_NAME_TEMP_EVE = "temp_eve";
        public static final String COLUMN_NAME_TEMP_MORN = "temp_morn";

    }

}
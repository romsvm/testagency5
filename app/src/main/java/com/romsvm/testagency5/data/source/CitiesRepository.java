package com.romsvm.testagency5.data.source;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.data.source.local.CitiesLocalDataSource;
import com.romsvm.testagency5.data.source.remote.RemoteData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 17.04.2017.
 */

public class CitiesRepository implements CitiesDataSource {

    private static CitiesRepository instance;

    public static CitiesRepository getInstance(CitiesLocalDataSource citiesLocalDataSource,
                                               RemoteData remoteData) {
        if (instance == null) {
            instance = new CitiesRepository(citiesLocalDataSource, remoteData);
        }

        return instance;
    }

    boolean isInited = false;
    boolean cacheIsDirty = true;
    List<City> cachedCities = new ArrayList<>(0);
    HashMap<Integer, List<Forecast>> cachedForecasts = new HashMap<>();

    private final CitiesLocalDataSource citiesLocalDataSource;
    private final RemoteData remoteData;

    public CitiesRepository(@NonNull CitiesLocalDataSource citiesLocalDataSource,
                            @NonNull RemoteData remoteData) {
        this.citiesLocalDataSource = checkNotNull(citiesLocalDataSource);
        this.remoteData = checkNotNull(remoteData);
    }

    /*При первом запуске приложения инициализирую список городов по умолчанию*/
    public synchronized void initData(final int[] cities, final InitDataCallback callback) {

        for (int i = 0; i < cities.length; i++) {

            final int currentIndex = i;
            final int cityId = cities[i];

            remoteData.getForecast(cityId, new RemoteData.RemoteGetForecastCallback() {
                @Override
                public void onForecastLoaded(City city, List<Forecast> listForecast) {
                    addCity(city, listForecast);

                    if (currentIndex == cities.length - 1) {
                        isInited = true;
                        callback.onInitiated();
                    }
                }

                /*Ничего не добавляю*/
                @Override
                public void onError() {
                    Log.d("MY", "initData onError");
                }
            });
        }
    }

    /*Проверяю есть ли город с введенным именем на сервере и если есть то добавляю в кеш и базу
    * данных*/
    @Override
    public void checkCity(String cityName, @NonNull final CheckCityCallback callback) {
        remoteData.getCityByName(cityName, new RemoteData.RemoteGetCityCallback() {
            @Override
            public void onCityLoaded(City city) {
                remoteData.getForecast(city.getId(), new RemoteData.RemoteGetForecastCallback() {
                    @Override
                    public void onForecastLoaded(City city, List<Forecast> listForecast) {
                        addCity(city, listForecast);
                        callback.onCityLoaded(city);
                    }

                    @Override
                    public void onError() {
                        callback.onDataNotAvailable();
                    }
                });
            }

            @Override
            public void onError() {
                callback.onDataNotAvailable();
            }
        });
    }

    /*Получаю список сохраненных в базе данных городов и обновляю все города из списка*/
    @Override
    public synchronized void getAllCities(@NonNull final GetAllCitiesCallback callback) {

        //Если загрузка не при старте приложения, то данные беру из кэша в памяти.
        if (!cacheIsDirty) {
            callback.onCitiesLoaded(new ArrayList<City>(cachedCities),
                    new HashMap<Integer, List<Forecast>>(cachedForecasts));
        } else {

            citiesLocalDataSource.getAllCities(new GetAllCitiesCallback() {
                @Override
                public void onCitiesLoaded(final List<City> citiesFromLocalDataSource,
                                           final HashMap<Integer, List<Forecast>> forecastsFromLocalDataSource) {

                    /*Если запуск на устройстве первый, то обновлять не нужно, т.к. все свежее после
                    * InitData*/
                    if (isInited) {
                        refreshCache(citiesFromLocalDataSource, forecastsFromLocalDataSource);
                        callback.onCitiesLoaded(new ArrayList<City>(cachedCities),
                                new HashMap<Integer, List<Forecast>>(cachedForecasts));
                    }
                    /*Если загрузка происходит при старте приложения и запуск на устройстве не первый,
                     то все передаю для обновления в RemoteData*/
                    else {
                        remoteData.updateAll(citiesFromLocalDataSource,
                                new RemoteData.RemoteUpdateAllCallback() {

                                    /*Если есть интернет и список городов получен, то обновляю базу
                                    данных и кеш в памяти*/
                                    @Override
                                    public void onUpdateCompleted(List<City> cities,
                                                                  HashMap<Integer, List<Forecast>> updatedForecasts) {
                                        refreshAll(cities, updatedForecasts);
                                        callback.onCitiesLoaded(new ArrayList<City>(cachedCities),
                                                new HashMap<Integer, List<Forecast>>(
                                                        cachedForecasts));
                                    }

                                    /*Если интернета нет или сервер недоступен, то работаю с локальными
                                    данными*/
                                    @Override
                                    public void onError() {
                                        refreshCache(citiesFromLocalDataSource,
                                                forecastsFromLocalDataSource);
                                        callback.onCitiesLoaded(new ArrayList<City>(cachedCities),
                                                new HashMap<Integer, List<Forecast>>(
                                                        cachedForecasts));
                                    }
                                });
                    }


                }

                /*Если база данных повреждена*/
                @Override
                public void onDataNotAvailable() {
                    callback.onDataNotAvailable();
                }
            });
        }
    }

    /*Возвращаю город с указанным id*/
    @Override
    public void getCity(@NonNull int cityId, @NonNull GetCityCallback callback) {

        City result = null;

        for (City city : cachedCities) {
            if (city.getId() == cityId) {
                result = city;
                break;
            }
        }

        if (result != null) {
            List<Forecast> list = cachedForecasts.get(result.getId());
            callback.onCityLoaded(result, list);
        } else {
            callback.onDataNotAvailable();
        }
    }

    /*Добавляю город введенный пользователем в базу данных*/
    //TODO добавление городов с одинаковым id
    @Override
    public synchronized void addCity(City city, List<Forecast> listForecast) {
        citiesLocalDataSource.addCity(city, listForecast);
        cachedCities.add(city);
        cachedForecasts.put(city.getId(), listForecast);
    }

    /*Меняю порядок города в списке*/
    @Override
    public synchronized void reorderCities(int fromPosition, int toPosition) {
        citiesLocalDataSource.reorderCities(fromPosition, toPosition);

        City replacedCity = cachedCities.get(fromPosition);
        cachedCities.remove(fromPosition);
        cachedCities.add(toPosition, replacedCity);
    }

    /*Удаляю город выбранный пользователем*/
    @Override
    public synchronized void removeCity(int cityId) {

        citiesLocalDataSource.removeCity(cityId);

        for (City city : cachedCities) {
            if (city.getId() == cityId) {
                cachedCities.remove(city);
                break;
            }
        }

        cachedForecasts.remove(cityId);
    }

    @Override
    public void removeAllCities() {
        cachedCities.clear();
        cachedForecasts.clear();
        citiesLocalDataSource.removeAllCities();
    }

    private void refreshAll(List<City> cities, HashMap<Integer, List<Forecast>> forecasts) {
        removeAllCities();

        for (City city : cities) {
            cachedCities.add(city);
            cachedForecasts.put(city.getId(), forecasts.get(city.getId()));
            citiesLocalDataSource.addCity(city, forecasts.get(city.getId()));
        }

        cacheIsDirty = false;
    }

    private void refreshCache(List<City> cities, HashMap<Integer, List<Forecast>> forecasts) {
        cachedCities.clear();
        cachedForecasts.clear();

        for (City city : cities) {
            cachedCities.add(city);
            cachedForecasts.put(city.getId(), forecasts.get(city.getId()));
        }

        cacheIsDirty = false;
    }
}

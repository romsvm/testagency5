package com.romsvm.testagency5.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.data.source.CitiesDataSource;
import com.romsvm.testagency5.data.source.DataPersistenceContract.CityEntry;
import com.romsvm.testagency5.data.source.DataPersistenceContract.ForecastEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 17.04.2017.
 */

public class CitiesLocalDataSource implements CitiesDataSource {

    private static CitiesLocalDataSource instance;

    public static CitiesLocalDataSource getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new CitiesLocalDataSource(context);
        }

        return instance;
    }

    private CitiesDbHelper citiesDbHelper;
    private Comparator<City> comparatorByOrder = new Comparator<City>() {
        @Override
        public int compare(City cityA, City cityB) {
            if (cityA.getOrder() > cityB.getOrder()) {
                return 1;
            } else if (cityA.getOrder() < cityB.getOrder()) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    private CitiesLocalDataSource(@NonNull Context context) {
        checkNotNull(context);
        citiesDbHelper = new CitiesDbHelper(context);
    }

    @Override
    public void checkCity(String cityName, @NonNull CheckCityCallback callback) {

    }

    @Override
    public void getAllCities(@NonNull GetAllCitiesCallback callback) {
        List<City> listCities = null;
        HashMap<Integer, List<Forecast>> mapForecasts = null;

        SQLiteDatabase db = citiesDbHelper.getReadableDatabase();

        Cursor cursorCities =
                db.query(CityEntry.TABLE_NAME, null, null, null, null, null, null);

        Cursor cursorForecasts =
                db.query(ForecastEntry.TABLE_NAME, null, null, null, null, null, null);

        List<Forecast> listAllForecast = new ArrayList<>();

        if (cursorForecasts != null && cursorForecasts.getCount() > 0) {
            while (cursorForecasts.moveToNext()) {
                listAllForecast.add(createForecast(cursorForecasts));
            }

            mapForecasts = new HashMap<>();
        }

        if (cursorCities != null && cursorCities.getCount() > 0) {
            listCities = new ArrayList<>();

            while (cursorCities.moveToNext()) {

                City city = createCity(cursorCities);
                listCities.add(city);

                List<Forecast> forecasts = new ArrayList<>();

                for (Forecast f : listAllForecast) {
                    if (city.getId() == f.getCityId()) {
                        forecasts.add(f);
                    }
                }

                mapForecasts.put(city.getId(), forecasts);
            }

            Collections.sort(listCities, comparatorByOrder);
        }

        db.close();

        if (listCities != null) {
            callback.onCitiesLoaded(listCities, mapForecasts);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void getCity(@NonNull int cityId,
                        @NonNull GetCityCallback callback) {
        SQLiteDatabase db = citiesDbHelper.getWritableDatabase();
        String[] selectionArgs = {String.valueOf(cityId)};
        String selection = CityEntry.COLUMN_NAME_ID + " LIKE ?";

        Cursor c = db.query(CityEntry.TABLE_NAME, null, selection, selectionArgs, null, null,
                null);

        City city = null;

        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            city = createCity(c);

            c.close();
        }

        selection = ForecastEntry.COLUMN_NAME_CITY_ID + " LIKE ?";
        c = db.query(ForecastEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        List<Forecast> listForecasts = null;

        if (c != null && c.getCount() > 0) {

            listForecasts = new ArrayList<>();

            while (c.moveToNext()) {
                Forecast forecast = createForecast(c);
                listForecasts.add(forecast);
            }
        }


        db.close();

        if (city != null && listForecasts != null) {
            callback.onCityLoaded(city, listForecasts);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void addCity(@NonNull City city, @NonNull List<Forecast> listForecast) {
        checkNotNull(city);

        SQLiteDatabase db = citiesDbHelper.getWritableDatabase();
        Cursor cursorCities =
                db.query(CityEntry.TABLE_NAME, null, null, null, null, null, null);
        if (cursorCities != null && cursorCities.getCount() > 0) {
            city.setOrder(cursorCities.getCount());
        } else {
            city.setOrder(0);
        }

        db.insertOrThrow(CityEntry.TABLE_NAME, null, createContentValuesCity(city));

        for (Forecast forecast : listForecast) {
            db.insertOrThrow(ForecastEntry.TABLE_NAME, null, createContentValuesForecast(forecast));
        }

        db.close();
    }

    @Override
    public void reorderCities(int fromPosition, int toPosition) {
        SQLiteDatabase db = citiesDbHelper.getWritableDatabase();
        Cursor cursorCities =
                db.query(CityEntry.TABLE_NAME, null, null, null, null, null, null);
        if (cursorCities != null && cursorCities.getCount() > 0) {
            List<City> listCities = new ArrayList<>();

            while (cursorCities.moveToNext()) {
                City city = createCity(cursorCities);
                listCities.add(city);
            }

            Collections.sort(listCities, comparatorByOrder);

            City replacedCity = listCities.get(fromPosition);
            listCities.remove(fromPosition);
            listCities.add(toPosition, replacedCity);

            for (int i = 0; i < listCities.size(); i++) {
                City city = listCities.get(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put(CityEntry.COLUMN_NAME_ORDER, i);
                String selection = CityEntry.COLUMN_NAME_ID + " LIKE ?";
                String[] args = {String.valueOf(city.getId())};
                db.update(CityEntry.TABLE_NAME, contentValues, selection, args);
            }
        }
    }

    @Override
    public void removeCity(int cityId) {
        SQLiteDatabase db = citiesDbHelper.getWritableDatabase();

        /*Удаляю город*/
        String[] selectionArgs = {String.valueOf(cityId)};
        String selection = CityEntry.COLUMN_NAME_ID + " LIKE ?";
        db.delete(CityEntry.TABLE_NAME, selection, selectionArgs);

        selection = ForecastEntry.COLUMN_NAME_CITY_ID + " LIKE ?";
        db.delete(ForecastEntry.TABLE_NAME, selection, selectionArgs);

        /*Меняю порядковые номера всех городов*/
        Cursor cursorCities =
                db.query(CityEntry.TABLE_NAME, null, null, null, null, null, null);
        if (cursorCities != null && cursorCities.getCount() > 0) {
            List<City> listCities = new ArrayList<>();

            while (cursorCities.moveToNext()) {
                City city = createCity(cursorCities);
                listCities.add(city);
            }

            Collections.sort(listCities, comparatorByOrder);

            for (int i = 0; i < listCities.size(); i++) {
                City city = listCities.get(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put(CityEntry.COLUMN_NAME_ORDER, i);
                selection = CityEntry.COLUMN_NAME_ID + " LIKE ?";
                String[] args = {String.valueOf(city.getId())};
                db.update(CityEntry.TABLE_NAME, contentValues, selection, args);
            }
        }

        db.close();
    }

    @Override
    public void removeAllCities() {
        SQLiteDatabase db = citiesDbHelper.getWritableDatabase();
        db.delete(CityEntry.TABLE_NAME, null, null);
        db.delete(ForecastEntry.TABLE_NAME, null, null);
        db.close();
    }

    private City createCity(Cursor cursorCities) {
        int id = cursorCities.getInt(
                cursorCities.getColumnIndexOrThrow(CityEntry.COLUMN_NAME_ID));
        int order = cursorCities
                .getInt(cursorCities.getColumnIndexOrThrow(CityEntry.COLUMN_NAME_ORDER));
        String name = cursorCities
                .getString(cursorCities.getColumnIndexOrThrow(CityEntry.COLUMN_NAME_NAME));
        String country = cursorCities
                .getString(cursorCities.getColumnIndexOrThrow(CityEntry.COLUMN_NAME_COUNTRY));
        double coord_lon = cursorCities
                .getDouble(cursorCities.getColumnIndexOrThrow(CityEntry.COLUMN_NAME_COORD_LON));
        double coord_lat = cursorCities
                .getDouble(cursorCities.getColumnIndexOrThrow(CityEntry.COLUMN_NAME_COORD_LAT));

        return new City.Builder()
                .setId(id)
                .setOrder(order)
                .setName(name)
                .setCoordLon(coord_lon)
                .setCoordLat(coord_lat)
                .setCountry(country)
                .build();
    }

    private Forecast createForecast(Cursor cursorForecast) {
        int cityId = cursorForecast.getInt(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_CITY_ID));
        int dt = cursorForecast.getInt(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_DT));
        int clouds = cursorForecast.getInt(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_CLOUDS));
        double pressure = cursorForecast
                .getDouble(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_PRESSURE));
        int humidity = cursorForecast.getInt(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_HUMIDITY));
        double temp_day = cursorForecast
                .getDouble(
                        cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_TEMP_DAY));
        double temp_min = cursorForecast
                .getDouble(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_TEMP_MIN));
        double temp_max = cursorForecast
                .getDouble(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_TEMP_MAX));
        double temp_night = cursorForecast
                .getDouble(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_TEMP_NIGHT));
        double temp_eve = cursorForecast
                .getDouble(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_TEMP_EVE));
        double temp_morn = cursorForecast.getDouble(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_TEMP_MORN));
        int weather_id = cursorForecast.getInt(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_WEATHER_ID));
        String weather_main = cursorForecast
                .getString(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_WEATHER_MAIN));
        String weather_description = cursorForecast
                .getString(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_WEATHER_DESCRIPTION));
        String weather_icon = cursorForecast
                .getString(cursorForecast
                        .getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_WEATHER_ICON));
        double wind_speed = cursorForecast
                .getDouble(
                        cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_SPEED));
        int wind_deg = cursorForecast.getInt(
                cursorForecast.getColumnIndexOrThrow(ForecastEntry.COLUMN_NAME_DEG));


        return new Forecast.Builder()
                .setCityId(cityId)
                .setDt(dt)
                .setPressure(pressure)
                .setHumidity(humidity)
                .setClouds(clouds)
                .setSpeed(wind_speed)
                .setDeg(wind_deg)

                .setTempDay(temp_day)
                .setTempMin(temp_min)
                .setTempMax(temp_max)
                .setTempMorn(temp_morn)
                .setTempEve(temp_eve)
                .setTempNight(temp_night)

                .setWeatherDescription(weather_description)
                .setWeatherIcon(weather_icon)
                .setWeatherId(weather_id)
                .setWeatherMain(weather_main)

                .build();
    }

    private ContentValues createContentValuesCity(City city) {
        ContentValues result = new ContentValues();
        result.put(CityEntry.COLUMN_NAME_ID, city.getId());
        result.put(CityEntry.COLUMN_NAME_ORDER, city.getOrder());
        result.put(CityEntry.COLUMN_NAME_NAME, city.getName());
        result.put(CityEntry.COLUMN_NAME_COORD_LAT, city.getCoord().getLat());
        result.put(CityEntry.COLUMN_NAME_COORD_LON, city.getCoord().getLon());
        result.put(CityEntry.COLUMN_NAME_COUNTRY, city.getCountry());

        return result;
    }

    private ContentValues createContentValuesForecast(Forecast forecast) {
        ContentValues result = new ContentValues();

        result.put(ForecastEntry.COLUMN_NAME_CITY_ID, forecast.getCityId());
        result.put(ForecastEntry.COLUMN_NAME_DT, forecast.getDt());
        result.put(ForecastEntry.COLUMN_NAME_CLOUDS, forecast.getClouds());
        result.put(ForecastEntry.COLUMN_NAME_PRESSURE, forecast.getPressure());
        result.put(ForecastEntry.COLUMN_NAME_HUMIDITY, forecast.getHumidity());
        result.put(ForecastEntry.COLUMN_NAME_SPEED, forecast.getSpeed());
        result.put(ForecastEntry.COLUMN_NAME_DEG, forecast.getDeg());

        result.put(ForecastEntry.COLUMN_NAME_TEMP_DAY, forecast.getTemp().getDay());
        result.put(ForecastEntry.COLUMN_NAME_TEMP_MAX, forecast.getTemp().getMax());
        result.put(ForecastEntry.COLUMN_NAME_TEMP_MIN, forecast.getTemp().getMin());
        result.put(ForecastEntry.COLUMN_NAME_TEMP_NIGHT, forecast.getTemp().getNight());
        result.put(ForecastEntry.COLUMN_NAME_TEMP_EVE, forecast.getTemp().getEve());
        result.put(ForecastEntry.COLUMN_NAME_TEMP_MORN, forecast.getTemp().getMorn());

        result.put(ForecastEntry.COLUMN_NAME_WEATHER_ID, forecast.getWeather().get(0).getId());
        result.put(ForecastEntry.COLUMN_NAME_WEATHER_MAIN,
                forecast.getWeather().get(0).getMain());
        result.put(ForecastEntry.COLUMN_NAME_WEATHER_DESCRIPTION,
                forecast.getWeather().get(0).getDescription());
        result.put(ForecastEntry.COLUMN_NAME_WEATHER_ICON,
                forecast.getWeather().get(0).getIcon());

        return result;
    }
}
package com.romsvm.testagency5.data.model;

public class City {

    public static class Builder {
        private int id;
        private int order;
        private String name;
        private String country;
        private double coord_lon;
        private double coord_lat;

        public Builder() {

        }

        public Builder setId(int value) {
            id = value;
            return this;
        }

        public Builder setOrder(int value) {
            order = value;
            return this;
        }

        public Builder setName(String value) {
            name = value;
            return this;
        }

        public Builder setCoordLon(double value) {
            coord_lon = value;
            return this;
        }

        public Builder setCoordLat(double value) {
            coord_lat = value;
            return this;
        }

        public Builder setCountry(String value) {
            country = value;
            return this;
        }

        public City build() {
            return new City(this);
        }

    }

    private int id;
    private int order;
    private String name;
    private Coord coord;
    private String country;

    public City(Builder builder) {
        id = builder.id;
        order = builder.order;
        name = builder.name;
        coord = new Coord();
        coord.setLat(builder.coord_lat);
        coord.setLon(builder.coord_lon);
        country = builder.country;
    }

    public int getId() {
        return id;
    }



    public void setId(int id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int value) {
        order = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return true;
        }
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return "City{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", coord=" + coord +
               ", country='" + country + '\'' +
               '}';
    }
}
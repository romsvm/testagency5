package com.romsvm.testagency5.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.romsvm.testagency5.data.source.DataPersistenceContract.CityEntry;
import com.romsvm.testagency5.data.source.DataPersistenceContract.ForecastEntry;

/**
 * Created by Roman on 18.04.2017.
 */

public class CitiesDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "TestAgency5.db";

    private static final String NOT_NULL = " NOT NULL";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CITIES_ENTRIES =
            "CREATE TABLE " + CityEntry.TABLE_NAME + " (" +
            CityEntry._ID + INTEGER_TYPE + NOT_NULL + " PRIMARY KEY" + COMMA_SEP +
            CityEntry.COLUMN_NAME_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            CityEntry.COLUMN_NAME_ORDER + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            CityEntry.COLUMN_NAME_COORD_LON + REAL_TYPE + NOT_NULL + COMMA_SEP +
            CityEntry.COLUMN_NAME_COORD_LAT + REAL_TYPE + NOT_NULL + COMMA_SEP +
            CityEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            CityEntry.COLUMN_NAME_NAME + TEXT_TYPE + NOT_NULL + " );";

    private static final String SQL_CREATE_FORECASTS_ENTRIES =
            "CREATE TABLE " + ForecastEntry.TABLE_NAME + " (" +
            ForecastEntry._ID + INTEGER_TYPE + NOT_NULL + " PRIMARY KEY" + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_CITY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_DT + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_CLOUDS + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_SPEED + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_PRESSURE + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_HUMIDITY + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_DEG + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_TEMP_DAY + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_TEMP_MAX + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_TEMP_MIN + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_TEMP_MORN + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_TEMP_NIGHT + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_TEMP_EVE + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_WEATHER_ID + REAL_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_WEATHER_MAIN + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_WEATHER_DESCRIPTION + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            ForecastEntry.COLUMN_NAME_WEATHER_ICON + TEXT_TYPE + NOT_NULL + " );";

    public CitiesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CITIES_ENTRIES);
        db.execSQL(SQL_CREATE_FORECASTS_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}



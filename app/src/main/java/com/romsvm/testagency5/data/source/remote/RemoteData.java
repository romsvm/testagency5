package com.romsvm.testagency5.data.source.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by romsvm on 17.04.2017.
 */

public class RemoteData {

    public static final int CNT_DAYS_FOR_REQUEST = 5;

    public interface RemoteUpdateAllCallback {
        void onUpdateCompleted(List<City> cities,
                               HashMap<Integer, List<Forecast>> updatedForecasts);

        void onError();
    }

    public interface RemoteGetCityCallback {
        void onCityLoaded(City city);

        void onError();
    }

    public interface RemoteGetForecastCallback {
        void onForecastLoaded(City city, List<Forecast> listForecast);

        void onError();
    }


    /*https://openweathermap.org/api*/
    interface OpenWeatherMapAPI {
        /*http://api.openweathermap.org/data/2.5/weather?q=London*/
        @GET("weather")
        Call<City> getCityByName(@Query("q") String cityName);

        /*http://api.openweathermap.org/data/2.5/forecast/daily?id=2172797&cnt=5*/
        @GET("forecast/daily")
        Call<ResponseGetForecast> getForecast(@Query("id") String cityId, @Query("cnt") int cnt);
    }

    private class ResponseGetForecast {
        List<Forecast> list;
        City city;
    }

    private static OpenWeatherMapAPI OPENWEATHERMAP_API;
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private static RemoteData instance;

    public static RemoteData getInstance() {
        if (instance == null) {
            instance = new RemoteData();
        }

        return instance;
    }

    private Retrofit retrofit;

    private RemoteData() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okHttpClient =
                new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();
                        HttpUrl originalHttpUrl = originalRequest.url();
                        HttpUrl newHttpUrl = originalHttpUrl.newBuilder()
                                                            .addQueryParameter("APPID",
                                                                    "0b357565301f6ab99a45137cb8c607d9")
                                                            .addQueryParameter("units", "metric")
                                                            .build();

                        Request.Builder builder = originalRequest.newBuilder().url(newHttpUrl);
                        Request newRequest = builder.build();

                        return chain.proceed(newRequest);
                    }
                }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        OPENWEATHERMAP_API = retrofit.create(OpenWeatherMapAPI.class);
    }

    public void updateAll(List<City> cities, final RemoteUpdateAllCallback callback) {

        final HashMap<Integer, List<Forecast>> updatedForecasts = new HashMap<>();
        final List<City> updatedCities = new ArrayList<>();

        for (int i = 0; i < cities.size(); i++) {

            City city = cities.get(i);

            getForecast(city.getId(), new RemoteGetForecastCallback() {
                @Override
                public void onForecastLoaded(City city, List<Forecast> listForecast) {
                    updatedCities.add(city);
                    updatedForecasts.put(city.getId(), listForecast);
                }

                @Override
                public void onError() {
                }
            });
        }

        if (cities.size() == updatedCities.size()) {
            callback.onUpdateCompleted(updatedCities, updatedForecasts);
        } else {
            callback.onError();
        }
    }

    public void getForecast(int cityId, RemoteGetForecastCallback callback) {
        Call<ResponseGetForecast> call = OPENWEATHERMAP_API.getForecast(String.valueOf(cityId),
                CNT_DAYS_FOR_REQUEST);

        try {
            Response<ResponseGetForecast> response = call.execute();

            if (response.isSuccessful()) {
                ResponseGetForecast responseGetForecast = response.body();

                for (Forecast forecast : responseGetForecast.list) {
                    forecast.setCityId(responseGetForecast.city.getId());
                }

                callback.onForecastLoaded(responseGetForecast.city, responseGetForecast.list);
            } else {
                callback.onError();
            }

        } catch (IOException e) {
            e.printStackTrace();
            callback.onError();
        }
    }

    public void getCityByName(String cityName, RemoteGetCityCallback callback) {
        Call<City> call = OPENWEATHERMAP_API.getCityByName(String.valueOf(cityName));

        try {
            Response<City> response = call.execute();

            if (response.isSuccessful()) {
                City result = response.body();
                callback.onCityLoaded(result);
            } else {
                callback.onError();
            }

        } catch (IOException e) {
            callback.onError();
        }
    }
}
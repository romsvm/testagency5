package com.romsvm.testagency5.data.model;

import java.util.ArrayList;
import java.util.List;

public class Forecast {

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public static class Builder {

        private double temp_day;
        private double temp_min;
        private double temp_max;
        private double temp_night;
        private double temp_eve;
        private double temp_morn;

        private int weather_id;
        private String weather_main;
        private String weather_description;
        private String weather_icon;

        private int cityId;
        private int dt;
        private double speed;
        private int deg;
        private int clouds;
        private double pressure;
        private int humidity;

        public Builder() {

        }

        public Builder setCityId(int value) {
            cityId = value;
            return this;
        }

        public Builder setDt(int value) {
            dt = value;
            return this;
        }

        public Builder setSpeed(double value) {
            speed = value;
            return this;
        }

        public Builder setDeg(int value) {
            deg = value;
            return this;
        }

        public Builder setClouds(int value) {
            clouds = value;
            return this;
        }

        public Builder setPressure(double value) {
            pressure = value;
            return this;
        }

        public Builder setHumidity(int value) {
            humidity = value;
            return this;
        }

        public Builder setWeatherId(int value) {
            weather_id = value;
            return this;
        }

        public Builder setWeatherMain(String value) {
            weather_main = value;
            return this;
        }

        public Builder setWeatherDescription(String value) {
            weather_description = value;
            return this;
        }

        public Builder setWeatherIcon(String value) {
            weather_icon = value;
            return this;
        }

        public Builder setTempDay(double value) {
            temp_day = value;
            return this;
        }

        public Builder setTempMin(double value) {
            temp_min = value;
            return this;
        }

        public Builder setTempMax(double value) {
            temp_max = value;
            return this;
        }

        public Builder setTempNight(double value) {
            temp_night = value;
            return this;
        }

        public Builder setTempEve(double value) {
            temp_eve = value;
            return this;
        }

        public Builder setTempMorn(double value) {
            temp_morn = value;
            return this;
        }

        public Forecast build() {
            return new Forecast(this);
        }

    }

    public Forecast(Builder builder) {
        this.cityId = builder.cityId;
        this.dt = builder.dt;
        this.pressure = builder.pressure;
        this.humidity = builder.humidity;
        this.speed = builder.speed;
        this.deg = builder.deg;
        this.clouds = builder.clouds;

        this.temp = new Temp();
        this.temp.setDay(builder.temp_day);
        this.temp.setNight(builder.temp_night);
        this.temp.setMorn(builder.temp_morn);
        this.temp.setEve(builder.temp_eve);
        this.temp.setMin(builder.temp_min);
        this.temp.setMax(builder.temp_max);

        weather = new ArrayList<>();
        Weather w = new Weather();
        w.setIcon(builder.weather_icon);
        w.setId(builder.weather_id);
        w.setMain(builder.weather_main);
        w.setDescription(builder.weather_description);
        weather.add(w);
    }

    private int cityId;
    private int dt;
    private Temp temp;
    private double pressure;
    private int humidity;
    private List<Weather> weather = null;
    private double speed;
    private int deg;
    private int clouds;

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public java.util.List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(java.util.List<Weather> weather) {
        this.weather = weather;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getDeg() {
        return deg;
    }

    public void setDeg(int deg) {
        this.deg = deg;
    }

    public int getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return true;
        }
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return "Forecast{" +
               "cityId=" + cityId +
               ", dt=" + dt +
               ", temp=" + temp.toString() +
               ", pressure=" + pressure +
               ", humidity=" + humidity +
               ", weather=" + weather.get(0).toString() +
               ", speed=" + speed +
               ", deg=" + deg +
               ", clouds=" + clouds +
               '}';
    }
}

package com.romsvm.testagency5.util;

import java.util.Calendar;

/**
 * Created by romsvm on 11.04.2017.
 */

public class DateUtils {
    public static long getToday() {
        Calendar calendarToday = Calendar.getInstance();
        calendarToday.set(Calendar.HOUR_OF_DAY, 0);
        calendarToday.set(Calendar.MINUTE, 0);
        calendarToday.set(Calendar.SECOND, 0);
        calendarToday.set(Calendar.MILLISECOND, 0);

        return calendarToday.getTimeInMillis();
    }

    public static long getTomorrow() {
        Calendar calendarToday = Calendar.getInstance();
        calendarToday.set(Calendar.HOUR_OF_DAY, 0);
        calendarToday.set(Calendar.MINUTE, 0);
        calendarToday.set(Calendar.SECOND, 0);
        calendarToday.set(Calendar.MILLISECOND, 0);
        calendarToday
                .set(Calendar.DAY_OF_MONTH, calendarToday.get(Calendar.DAY_OF_MONTH) + 1);

        return calendarToday.getTimeInMillis();
    }
}

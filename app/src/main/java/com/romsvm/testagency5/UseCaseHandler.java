package com.romsvm.testagency5;

import com.romsvm.testagency5.util.EspressoIdlingResource;

/**
 * Created by romsvm on 17.04.2017.
 */

public class UseCaseHandler {

    private static UseCaseHandler INSTANCE;

    public static UseCaseHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UseCaseHandler(new UseCaseThreadPoolScheduler());
        }

        return INSTANCE;
    }

    private UseCaseHandler(UseCaseScheduler useCaseScheduler) {
        this.useCaseScheduler = useCaseScheduler;
    }

    private final UseCaseScheduler useCaseScheduler;

    public <T extends UseCase.RequestValues, R extends UseCase.ResponseValue> void execute(
            final UseCase<T, R> useCase, T values, UseCase.UseCaseCallback<R> callback) {
        useCase.setRequestValues(values);
        /*С помощью декоратора передаем колбэк через useCaseHandler для поста из Handler внутри UseCaseScheduler в UIThread*/
        useCase.setUseCaseCallback(new UiCallbackWrapper(callback, this));

        EspressoIdlingResource.increment();

        useCaseScheduler.execute(new Runnable() {
            @Override
            public void run() {
                useCase.run();

                if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                    EspressoIdlingResource.decrement();
                }
            }
        });
    }

    private <V extends UseCase.ResponseValue> void notifyResponse(final V response,
                                                                  final UseCase.UseCaseCallback<V> useCaseCallback) {
        useCaseScheduler.notifyResponse(response, useCaseCallback);
    }

    private <V extends UseCase.ResponseValue> void notifyError(
            final UseCase.UseCaseCallback<V> useCaseCallback) {
        useCaseScheduler.onError(useCaseCallback);
    }

    private static final class UiCallbackWrapper<V extends UseCase.ResponseValue>
            implements UseCase.UseCaseCallback<V> {
        public UiCallbackWrapper(UseCase.UseCaseCallback<V> callback,
                                 UseCaseHandler useCaseHandler) {
            this.callback = callback;
            this.useCaseHandler = useCaseHandler;
        }

        private final UseCase.UseCaseCallback<V> callback;
        private final UseCaseHandler useCaseHandler;

        @Override
        public void onSuccess(V response) {
            useCaseHandler.notifyResponse(response, callback);
        }

        @Override
        public void onError() {
            useCaseHandler.notifyError(callback);
        }
    }
}



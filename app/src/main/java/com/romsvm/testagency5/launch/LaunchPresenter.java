package com.romsvm.testagency5.launch;

import android.support.annotation.NonNull;
import android.util.Log;

import com.romsvm.testagency5.UseCase;
import com.romsvm.testagency5.UseCaseHandler;
import com.romsvm.testagency5.allcities.domain.usecase.GetAllCities;
import com.romsvm.testagency5.allcities.domain.usecase.InitData;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by romsvm on 24.04.2017.
 */

public class LaunchPresenter implements LaunchContract.Presenter {

    private final UseCaseHandler useCaseHandler;
    private LaunchContract.View launchView;
    private final GetAllCities ucGetAllCities;
    private final InitData ucInitData;
    private boolean isInitDataInProgress = false;
    private boolean isGetAllCitiesInProgress = false;

    public LaunchPresenter(@NonNull UseCaseHandler useCaseHandler,
                           @NonNull LaunchContract.View launchView,
                           @NonNull GetAllCities ucGetAllCities,
                           @NonNull InitData ucInitData) {
        this.useCaseHandler = checkNotNull(useCaseHandler);
        this.launchView = checkNotNull(launchView);
        this.ucGetAllCities = checkNotNull(ucGetAllCities);
        this.ucInitData = checkNotNull(ucInitData);

        this.launchView.setPresenter(this);
    }

    @Override
    public void start() {
    }

    @Override
    public void updateAllCities() {
        if (!isGetAllCitiesInProgress) {
            isGetAllCitiesInProgress = true;
            useCaseHandler.execute(ucGetAllCities, new GetAllCities.RequestValues(),
                    new UseCase.UseCaseCallback<GetAllCities.ResponseValue>() {
                        @Override
                        public void onSuccess(GetAllCities.ResponseValue response) {

                            if (launchView.isActive()) {
                                launchView.showAllCities();
                            }

                            isGetAllCitiesInProgress = false;
                        }

                        @Override
                        public void onError() {
                            isGetAllCitiesInProgress = false;
                        }
                    });
        } else {
            Log.d("MY", "getAllCities уже выполняется");
        }
    }

    @Override
    public void initData(int[] cities) {

        if (!isInitDataInProgress) {
            isInitDataInProgress = true;
            useCaseHandler.execute(ucInitData, new InitData.RequestValues(cities),
                    new UseCase.UseCaseCallback<InitData.ResponseValue>() {
                        @Override
                        public void onSuccess(InitData.ResponseValue response) {
                            updateAllCities();
                            isInitDataInProgress = false;
                        }

                        /*При отсутствии интернета или недоступном сервере список остается пустым*/
                        @Override
                        public void onError() {
                            isInitDataInProgress = false;
                        }
                    });
        } else {
            Log.d("MY", "initData уже выполняется");
        }


    }
}

package com.romsvm.testagency5.launch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.romsvm.testagency5.R;
import com.romsvm.testagency5.allcities.AllCitiesActivity;

/**
 * Created by romsvm on 24.04.2017.
 */

public class LaunchFragment extends Fragment implements LaunchContract.View {
    public static LaunchFragment newInstance() {
        return new LaunchFragment();
    }

    private LaunchContract.Presenter presenter;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_launch, container, false);

        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);

        presenter.start();

        SharedPreferences sharedPreferences =
                getActivity()
                        .getSharedPreferences("com.romsvm.testagency5", Context.MODE_PRIVATE);
        String storedFirstLoad = sharedPreferences.getString("firstLoad", null);

        if (storedFirstLoad == null) {
            int[] cities = getResources().getIntArray(R.array.default_cities);

            presenter.initData(cities);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("firstLoad", "true");
            editor.apply();
        } else {
            presenter.updateAllCities();
        }

        return root;
    }

    @Override
    public void setPresenter(LaunchContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showAllCities() {
        Intent intent = new Intent(getContext(), AllCitiesActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }
}

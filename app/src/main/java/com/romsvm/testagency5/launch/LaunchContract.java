package com.romsvm.testagency5.launch;

import com.romsvm.testagency5.BasePresenter;
import com.romsvm.testagency5.BaseView;

/**
 * Created by romsvm on 24.04.2017.
 */

public interface LaunchContract {
    interface View extends BaseView<LaunchContract.Presenter> {
        void showAllCities();

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        /*Обновляю весь список городов*/
        void updateAllCities();

        /*Инициализирую список городов по-умолчанию.*/
        void initData(int[] cities);
    }
}
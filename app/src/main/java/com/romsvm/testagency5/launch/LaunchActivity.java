package com.romsvm.testagency5.launch;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;

import com.romsvm.testagency5.Injection;
import com.romsvm.testagency5.R;
import com.romsvm.testagency5.util.ActivityUtils;
import com.romsvm.testagency5.util.EspressoIdlingResource;

/**
 * Created by romsvm on 24.04.2017.
 */

public class LaunchActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        LaunchFragment launchFragment =
                (LaunchFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.contentFrame);
        if (launchFragment == null) {
            // Create the fragment
            launchFragment = LaunchFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), launchFragment, R.id.contentFrame);
        }

        new LaunchPresenter(Injection.provideUseCaseHandler(), launchFragment,
                Injection.provideGetAllCities(getApplicationContext()),
                Injection.provideInitData(getApplicationContext()));
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}

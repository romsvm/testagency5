package com.romsvm.testagency5;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.romsvm.testagency5.data.model.City;
import com.romsvm.testagency5.data.model.Forecast;
import com.romsvm.testagency5.data.source.CitiesDataSource;
import com.romsvm.testagency5.data.source.local.CitiesLocalDataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LocalDataSourceTest {

    private CitiesLocalDataSource citiesLocalDataSource;

    @Before
    public void setup() {
        citiesLocalDataSource =
                CitiesLocalDataSource.getInstance(InstrumentationRegistry.getTargetContext());
    }

    @After
    public void cleanUp() {
        citiesLocalDataSource.removeAllCities();
    }

    private City getTestCity() {
        return new City.Builder()
                .setId(1)
                .setName("Moskva")
                .setCoordLon(100)
                .setCoordLat(50)
                .setCountry("RU")
                .build();
    }

    private Forecast getTestForecast() {
        return new Forecast.Builder()
                .setCityId(1)
                .setDt(1)
                .setPressure(15.8)
                .setHumidity(5)
                .setClouds(2)

                .setTempDay(10)
                .setTempEve(1)
                .setTempMax(9)
                .setTempMin(0.2)
                .setTempNight(10.3)
                .setTempMorn(15.2)

                .setSpeed(6.3)
                .setDeg(89)

                .setWeatherDescription("RRR")
                .setWeatherIcon("fads")
                .setWeatherId(4)
                .setWeatherMain("main")

                .build();
    }

    @Test
    public void saveCity_retrieveCity() {

        final City savedCity = getTestCity();
        final Forecast forecast = getTestForecast();

        List<Forecast> list = new ArrayList<>();
        list.add(forecast);

        citiesLocalDataSource.addCity(savedCity, list);

        citiesLocalDataSource.getCity(savedCity.getId(), new CitiesDataSource.GetCityCallback() {
            @Override
            public void onCityLoaded(City city, List<Forecast> listForecasts) {
                assertThat(savedCity, is(city));
                assertThat(forecast, is(listForecasts.get(0)));
            }

            @Override
            public void onDataNotAvailable() {
                fail("onDataNotAvailable");
            }
        });
    }

    @Test
    public void deleteCity_noRetrievedCityAndForecast() {
        final City savedCity = getTestCity();
        final Forecast forecast = getTestForecast();

        List<Forecast> list = new ArrayList<>();
        list.add(forecast);

        citiesLocalDataSource.addCity(savedCity, list);
        citiesLocalDataSource.removeCity(savedCity.getId());

        CitiesDataSource.GetCityCallback callback = mock(CitiesDataSource.GetCityCallback.class);
        citiesLocalDataSource.getCity(savedCity.getId(), callback);

        verify(callback).onDataNotAvailable();
        verify(callback, never()).onCityLoaded(any(City.class), anyList());
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.romsvm.testagency5", appContext.getPackageName());
    }
}